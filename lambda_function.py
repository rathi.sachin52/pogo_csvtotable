import boto3
# from botocore.client import Config
# import pandas as pd
import urllib.request
import time
import os

# import psycopg2

poller_data = [
    {
        # 'url' : 'http://localhost:8000/Hurricane_Harvey_Report.xls',
        'url': 'https://www.fpds.gov/downloads/top_requests/Hurricane_Harvey_Report.xls',
        'sheets': [
            {'name': 'Hurricane Harvey Summary', 'nan_thresh': 10},
            {'name': 'Hurricane Harvey Contracts', 'nan_thresh': 10},
        ]
    }
]

# Configuration Data.. could be potentially moved to environment

# AWS_ACCESS_KEY_ID = 'AKIAIRVS4RXZLGMF2JTQ' for dev server
# AWS_ACCESS_SECRET_KEY = '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'
AWS_ACCESS_KEY_ID = 'AKIAJVUPVFI364TZBCUA'
AWS_ACCESS_SECRET_KEY = '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'
AWS_REGION_NAME = 'us-east-2'
AWS_S3_CSV_BUCKET_NAME = 'pogo1-harvey-csv-files'
AWS_S3_XLS_BUCKET_NAME = 'pogo1-harvey-xls-files'


def get_proxy( proxyType = 'HTTP'):
    httpproxykeys = ['HTTP_PROXY', 'http_proxy']
    httpsproxykeys = ['HTTPS_PROXY', 'https_proxy']

    keys = httpproxykeys if proxyType == 'HTTP' else httpsproxykeys

    for key in keys:
        if os.environ.get(key, None) is not None:
            return os.environ[key]

    return None


def save2s3bucket(source_file_path, bucket_name, dest_filename):
    # S3 Connect
    from botocore.config import Config
    config = Config(proxies={'https': get_proxy('HTTPS')}, region_name='us-east-2')
    s3 = boto3.resource(
        's3',
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_ACCESS_SECRET_KEY,
        config=config
    )

    # Upload File
    s3.Bucket(bucket_name).upload_file(source_file_path, dest_filename)


# def pull_data():
#     for source in poller_data:
#         file_url = source['url']
#
#         # URL of the file to be downloaded is defined as file_url
#         proxy = urllib.request.ProxyHandler({'http': get_proxy('HTTP'), 'https': get_proxy('HTTPS')})
#         # construct a new opener using your proxy settings
#         opener = urllib.request.build_opener(proxy)
#         # install the openen on the module-level
#         urllib.request.install_opener(opener)
#         local_filename, headers = urllib.request.urlretrieve(file_url)
#         xls_s3_filename = file_url.split("/")[-1].split('.')[0] + "_" + str(int(round(time.time() * 1000))) + ".xls"
#         save2s3bucket(local_filename, AWS_S3_XLS_BUCKET_NAME, xls_s3_filename)
#         # r = requests.get(harvey_report_xls)  # create HTTP response object
#         for sheet in source['sheets']:
#             df = pd.read_excel(local_filename, sheet_name=sheet['name'])
#             # drop unwanted rows based on NaN threshold setting
#             df = df.dropna(thresh=sheet['nan_thresh'])
#             # Update column names to column names from row 0
#             df.columns = df.values[0]
#             df.columns = df.columns.str.replace('\s+', '_')
#             # Drop Row 0 now .. earlier containing column header names
#             df = df.drop(df.index[0])
#             # Remove Row containing 'Total' in first column
#             df = df[df[df.columns[0]] != 'Total']
#             # Save the rest to csv
#             csv_file_path = '/tmp/' + sheet['name'].replace(' ', '_') + '.csv'
#             df.to_csv(csv_file_path, encoding='utf-8', index=False)
#             save2s3bucket(csv_file_path, AWS_S3_CSV_BUCKET_NAME, sheet['name'].replace(' ', '_') + '.csv')
#             print('Done parsing sheet')
#
#     print("Done.")


# code for grant excel convert in dataframe via pandas
def pull_data():
    # Import pandas
    import pandas as pd

    # Assign spreadsheet filename to `file`
    file = 'HurricaneHarveySept14-2018.xlsx'

    # Load spreadsheet
    xl = pd.ExcelFile(file)

    # Print the sheet names
    print(xl.sheet_names)

    # Load a sheet into a DataFrame by name: df1
    df1 = xl.parse('Sheet1')

# code for grant excel convert in csv


if __name__ == '__main__':
    pull_data()
