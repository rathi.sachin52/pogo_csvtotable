
from sqlalchemy import create_engine
import io
import errno
import os
import psycopg2 as me


class Config(object):

    # prodconnection
    POSTGRESQLDB_SETTINGS = {
        'host': 'pogo-admin.cyss2e3vn6ml.us-east-1.rds.amazonaws.com',
        'user': 'pogoadmin',
        'password': 'Vmv6NRhzoNWV6uhh',
        'dbname': 'pogo_dev'
    }


db_config = Config.POSTGRESQLDB_SETTINGS

conn = me.connect(**db_config)

# import psycopg2

poller_data = [
    {
        'url' : 'csv',

        'sheets': [
            {'name': 'HurricaneHarveySept14-2018'},
            {'name': 'HurricaneHarveySept14-2018_sheet2'},
            {'name': 'HurricaneHarveySept14-2018_sheet3'},
        ]
    }
]

# Configuration Data.. could be potentially moved to environment


def get_proxy( proxyType = 'HTTP'):
    httpproxykeys = ['HTTP_PROXY', 'http_proxy']
    httpsproxykeys = ['HTTPS_PROXY', 'https_proxy']

    keys = httpproxykeys if proxyType == 'HTTP' else httpsproxykeys

    for key in keys:
        if os.environ.get(key, None) is not None:
            return os.environ[key]

    return None


# code for grant excel convert in dataframe via pandas
def pull_data():
    import pandas as pd
    import numpy as np
    for source in poller_data:
            file_url = source['url']

            for sheet in source['sheets']:
                csv_file_path = 'csv/' + sheet['name'] + '.csv'
                df = pd.read_csv(csv_file_path)

                df_filterd = df.filter(items=['total_funding_amount','face_value_of_loan','awarding_agency_name', 'awarding_sub_agency_name','recipient_name','cfda_title',
 'award_description','award_id_fain','action_date', 'recipient_county_name','recipient_duns',
 'recipient_address_line_1', 'recipient_city_name','recipient_state_code', 'recipient_zip_code', 'primary_place_of_performance_city_name',
'primary_place_of_performance_county_name', 'cfda_number', 'recipient_country_name',
'assistance_type_code',  'business_types_code'])
                df_filterd.fillna(0, inplace=True)
                df_filterd['amount'] = np.where(df_filterd['face_value_of_loan']==0,
                                                df_filterd['total_funding_amount'], df_filterd['face_value_of_loan'])
                df_filterd = df_filterd.filter(
                    items=['amount', 'awarding_agency_name', 'awarding_sub_agency_name','recipient_name','cfda_title',
 'award_description','award_id_fain', 'action_date', 'recipient_county_name','recipient_duns',
 'recipient_address_line_1', 'recipient_city_name', 'recipient_state_code', 'recipient_zip_code', 'primary_place_of_performance_city_name',
'primary_place_of_performance_county_name', 'cfda_number', 'recipient_country_name',
'assistance_type_code',  'business_types_code'])

                df_filterd["award_description"] = df_filterd["award_description"].str[:2990]

                cols = ['amount']
                df_filterd[cols] = df_filterd[df_filterd[cols] > 0][cols]
                df_final_filterd = df_filterd.dropna()
                # 'recipient_parent_name', 'recipient_parent_duns',
                df_final_filterd.columns = ['federal_action_obligation','awarding_agency_name', 'awarding_sub_agency_name','recipient_name','cfda_title',
 'award_description','award_id_fain','action_date', 'recipient_county_name','recipient_duns',
 'recipient_address_line_1', 'recipient_city_name','recipient_state_name', 'recipient_zip_code', 'primary_place_of_performance_city_name',
'primary_place_of_performance_county_name', 'cfda_number', 'recipient_country_name',
'assistance_type_code',  'business_type']
                insert_in_table(df_final_filterd)


                print('Done parsing sheet')


def insert_in_table(df):

    cur = conn.cursor()
    output = io.StringIO()
    df.to_csv(output, sep='\t', header=False, index=False)
    output.seek(0)
    contents = output.getvalue()
    cur.copy_from(output, 'hurricane_harvey_grants', null="")  # null values become ''
    conn.commit()


if __name__ == '__main__':
    pull_data()
